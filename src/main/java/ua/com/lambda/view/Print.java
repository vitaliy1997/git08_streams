package ua.com.lambda.view;

@FunctionalInterface
public interface Print {
    public  void print();
}
