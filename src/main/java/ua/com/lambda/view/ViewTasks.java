package ua.com.lambda.view;

import ua.com.lambda.taskone.MainOne;
import ua.com.lambda.taskthree.TaskThree;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewTasks {

    private Map<String, String> menu = new LinkedHashMap<>();
    private Map<String, Print> methodsMenu = new LinkedHashMap<>();
    private MainOne mainOne = new MainOne();
    private TaskThree taskThree = new TaskThree();
    private Scanner input = new Scanner(System.in);
    private ViewTaskThree viewTaskThree = new ViewTaskThree();

    public ViewTasks() {

        menu.put("1", "2-(TaskOne)");
        menu.put("2", "2-Task 2");
        menu.put("3", "3-Task 3");
        menu.put("E", "E-Exit");

        methodsMenu.put("1", this::Press1);
        methodsMenu.put("2", this::Press2);
        methodsMenu.put("3", this::Press3);

    }

    private void Press1() {
        mainOne.TaskOneMax();

    }

    private void Press2() {

    }


    private void Press3() {
        taskThree.numListOne();
    }

    //---------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("E"));
    }

}