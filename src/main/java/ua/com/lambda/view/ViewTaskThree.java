package ua.com.lambda.view;

import ua.com.lambda.taskone.MainOne;
import ua.com.lambda.taskthree.TaskThree;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewTaskThree {

    Map<String, String> menu = new LinkedHashMap<>();
    Map<String, Print> putmenu = new LinkedHashMap<>();
    TaskThree taskThree = new TaskThree();
    Scanner scanner = new Scanner(System.in);

    public ViewTaskThree() {

        menu.put("1", "First task");
        menu.put("2", "Second task");
        menu.put("Q", "Q-Exit");

        putmenu.put("1", this::Button1);
        putmenu.put("2", this::Button2);
    }

    private void Button2() {

    }

    private void Button1() {
    }


    //---------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                putmenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
