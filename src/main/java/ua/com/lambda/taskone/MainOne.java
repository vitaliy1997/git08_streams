package ua.com.lambda.taskone;

import java.util.Optional;

public class MainOne {

    public  void TaskOneMax() {
        TaskOne taskOne = (a, b, c) -> {

            int max = Math.max(Math.max(a, b), c);
            System.out.println(max);
        };
        taskOne.method(4, 8, 3);
    }
//
//    public void TaskOneAverage(){
//        int num=3;
//        TaskOne taskOneAve = ((a, b, c) -> (a+b+c));
//    }

}
