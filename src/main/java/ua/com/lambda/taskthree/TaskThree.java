package ua.com.lambda.taskthree;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


public class TaskThree {

    public void numListOne() {
        List<Integer> list = Arrays.asList(new Integer[]{2, 4, 7, 12, 16, 17, 19, 20, 34, 56});

        Optional<Integer> max = list.stream().max(Integer::compareTo);
        Optional<Integer> min = list.stream().min(Integer::compareTo);
        Optional<Integer> sum = list.stream().reduce((left,right)->left+right);
        System.out.println("Max"+max);
        System.out.println("Min"+min);
        System.out.println("Sum"+sum);

    }

}
